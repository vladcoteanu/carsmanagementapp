function showAddCompany(){
	document.getElementById('add_company').style.display='block';
	document.getElementById('display_company').style.display='none';
	return false;
}

function showListCompanies(){
	document.getElementById('add_company').style.display='none';
	document.getElementById('display_company').style.display='block';
	document.getElementById('succesMessage').style.display='none';
    document.getElementById('failMessage').style.display='none';
	return false;
}

function addCompany() {
    var companyName = $('#insertCompanyName').val();
    var companyAdress = $('#insertCompanyAdress').val();
    var companyPostalCode = $('#insertPostalCode').val();


    if(document.getElementById("insertCompanyName").value == "") {
        document.getElementById('succesMessage').style.display='none';
        document.getElementById('failMessage').innerHTML="Please insert a company name";
        document.getElementById('failMessage').style.display='block';
        return false;
    }

    if(document.getElementById("insertCompanyAdress").value == "") {
        document.getElementById('succesMessage').style.display='none';
        document.getElementById('failMessage').innerHTML="Please insert a company adress";
        document.getElementById('failMessage').style.display='block';
        return false;
    }

    if(document.getElementById("insertPostalCode").value == "") {
       document.getElementById('succesMessage').style.display='none';
       document.getElementById('failMessage').innerHTML="Please insert a postal code";
       document.getElementById('failMessage').style.display='block';
       return false;
    }

    $.ajax({
        type: "POST",
        url: "/companies/addCompany",
        data: {
            "companyName" : companyName,
            "companyAdress" : companyAdress,
            "companyPostalCode": companyPostalCode
        },
        dataType: "json"
    }).done(function() {
              $('#insertCompanyName').val("");
              $('#insertCompanyAdress').val("");
              $('#insertPostalCode').val("");
              $('#failMessage').hide();
              $('#succesMessage').text("Company inserted");
              $('#succesMessage').show();
          }).fail(function() {
              $('#succesMessage').hide();
              $('#failMessage').text("Error");
              $('#failMessage').show();
          });
}

function updateCompany(id) {
    var companyId = id;
    var companyName = $('#updateCompanyName').val();
    var companyAdress = $('#updateCompanyAdress').val();
    var companyPostalCode    = $('#updatePostalCode').val();


        if(document.getElementById("updateCompanyName").value == "") {
            document.getElementById('succesUpMessage').style.display='none';
            document.getElementById('failUpMessage').innerHTML="Please insert a car name";
            document.getElementById('failUpMessage').style.display='block';
            return false;
        }

        if(document.getElementById("updateCompanyAdress").value == "") {
            document.getElementById('succesUpMessage').style.display='none';
            document.getElementById('failUpMessage').innerHTML="Please insert a car producer Id";
            document.getElementById('failUpMessage').style.display='block';
            return false;
        }

        if(document.getElementById("updatePostalCode").value == "") {
            document.getElementById('succesUpMessage').style.display='none';
            document.getElementById('failUpMessage').innerHTML="Please insert a car production year";
            document.getElementById('failUpMessage').style.display='block';
            return false;
        }


        $.ajax({
            type: "POST",
            url: '/companies/update',
            data: {
                "id" : companyId,
                "companyName" : companyName,
                "companyAdress" : companyAdress,
                "companyPostalCode" : companyPostalCode,
            },
            dataType: "json"
        }).done(function() {
                        $('#returnButton').show();
                    }).fail(function() {
                        $('#failMessage').text("Error");
                        $('#failMessage').show();
                    });
};