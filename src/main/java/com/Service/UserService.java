package com.Service;

import com.Model.User;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vlad on 7/21/2017.
 */
public interface UserService {

    @Transactional(readOnly = true)
    User getByUsername(String username);

}
