package com.Service;

import com.Model.User;
import com.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vlad on 7/21/2017.
 */
@Service
public class UserServiceImpl implements UserService{
    @Autowired
    UserRepository userRepository;

    @Transactional(readOnly = true)
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

}
