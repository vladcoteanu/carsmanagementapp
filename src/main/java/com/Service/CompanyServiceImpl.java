package com.Service;

import com.Model.CarsManufactures;
import com.Repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vlad on 7/14/2017.
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public void saveAndFlush(CarsManufactures carsManufactures){
        companyRepository.saveAndFlush(carsManufactures);
    }

    public List<CarsManufactures> list(){
        return companyRepository.findAll();
    }

    @Override
    public CarsManufactures getCompanyById(Long id) {
        return companyRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        companyRepository.delete(id);
    }
}
