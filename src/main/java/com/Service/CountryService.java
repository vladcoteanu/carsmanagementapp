package com.Service;

import com.Model.Country;

import java.util.List;

/**
 * Created by Vlad on 7/14/2017.
 */
public interface CountryService {
    public void saveAndFlush(Country country);
    public List<Country> list();
    public Country getCountryById(Long id);
    public void delete(Long id);
}
