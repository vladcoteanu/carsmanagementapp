package com.Service;

import com.Model.Cars;
import com.Repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vlad on 7/13/2017.
 */
@Service
public class CarServiceImpl implements CarService {
    @Autowired
    private CarRepository carRepository;

    public void saveAndFlush(Cars cars){
        carRepository.saveAndFlush(cars);
    }

    @Override
    public List<Cars> list() {
        return carRepository.findAll();
    }

    public CarServiceImpl() {}

    public Cars getCarById(Long id) {
        return carRepository.findOne(id);
    }

    @Override
    public void flush(Cars cars) {
        carRepository.save(cars);
    }

    @Override
    public void delete(Long id) {
        carRepository.delete(id);
    }
}
