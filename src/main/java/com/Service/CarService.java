package com.Service;

import com.Model.Cars;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vlad on 7/13/2017.
 */
public interface CarService {
    public void saveAndFlush(Cars cars);
    public List<Cars> list();
    public Cars getCarById(Long id);
    public void flush(Cars cars);
    public void delete(Long id);
}
