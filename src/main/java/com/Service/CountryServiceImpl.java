package com.Service;

import com.Model.Country;
import com.Repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vlad on 7/14/2017.
 */
@Service
public class CountryServiceImpl implements CountryService{

    @Autowired
    CountryRepository countryRepository;

    public void saveAndFlush(Country country) {
        countryRepository.saveAndFlush(country);
    }

    public List<Country> list(){
        return countryRepository.findAll();
    }

    @Override
    public Country getCountryById(Long id) {
        return countryRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        countryRepository.delete(id);
    }
}
