package com.Service;

import com.Model.CarsManufactures;

import java.util.List;

/**
 * Created by Vlad on 7/14/2017.
 */
public interface CompanyService {
    public void saveAndFlush(CarsManufactures carsManufactures);
    public List<CarsManufactures> list();
    public CarsManufactures getCompanyById(Long id);
    public void delete(Long id);
}
