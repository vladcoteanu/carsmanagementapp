package com.Controller;

import com.Model.Cars;
import com.Model.CarsManufactures;
import com.Service.CarService;
import com.Service.CarServiceImpl;
import com.Service.CompanyService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Vlad on 7/17/2017.
 */
@Controller
@RequestMapping(value="/cars")
public class CarsController {
    @Autowired
    CarService carService;
    @Autowired
    CompanyService companyService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String cars(Model model) {
        model.addAttribute("carsList", carService.list());
        model.addAttribute("companyList", companyService.list());
        return "cars";
    }


    @RequestMapping(value="/delete", method = RequestMethod.GET, produces = "application/json")
    public String deleteCar(@RequestParam("carId") String carId, Model model) {
        try {
            Long idToDelete = Long.parseLong(carId);
            carService.delete(idToDelete);
            return "redirect:/cars";
        } catch (Exception e) {
            return "THIS CAR STILL APPEARS IN SALES RECORDS";
        }
    }

    @RequestMapping(value="/update", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int updateCarToEdit(@ModelAttribute("id") Long id, @ModelAttribute("carName") String carName,
             @ModelAttribute("carProducerId") String carProducerId, @ModelAttribute("carRating") String carRating,
             @ModelAttribute("carProductionYear") String carProductionYear){
        try {
            int i = 0;
            long prodId = 0;
            while(carProducerId.charAt(i) != '.') {
                prodId = prodId * 10 + carProducerId.charAt(i) - 48;
                i++;
            }
            CarsManufactures carsManufactures = companyService.getCompanyById(prodId);
            Cars car = new Cars(carName, carsManufactures, Integer.parseInt(carProductionYear),
                    Double.parseDouble(carRating));
            Cars car2 = carService.getCarById(id);
            car.setCarId(car2.getCarId());
            BeanUtils.copyProperties(car, car2);
            carService.flush(car2);
            return HttpServletResponse.SC_OK;
        } catch (Exception e) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
    }


    @RequestMapping(value = "/editCar", method = RequestMethod.GET)
    public String displayCarToEdit(@RequestParam("carId") Long id, Model model) {
        Cars car = carService.getCarById(id);
        model.addAttribute("carToEdit",car);
        model.addAttribute("companyList", companyService.list());
        return "editCar";
    }

    @RequestMapping(value= "/addCar", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int AddUser(@ModelAttribute("carName") String carName, @ModelAttribute("carProducerId") String carProducerId,
                        @ModelAttribute("carProductionYear") String carProductionYear, @ModelAttribute("carRating")
                                    String carRating) {

        try {
            int i = 0;
            long prodId = 0;
            while(carProducerId.charAt(i) != '.') {
                prodId = prodId * 10 + carProducerId.charAt(i) - 48;
                i++;
            }
            CarsManufactures carsManufactures = companyService.getCompanyById(prodId);
            Cars cars = new Cars(carName, carsManufactures, Integer.parseInt(carProductionYear), Double.parseDouble(carRating));
            carService.saveAndFlush(cars);
            return HttpServletResponse.SC_OK;
        } catch (Exception e) {
            e.printStackTrace();
            return HttpServletResponse.SC_NOT_FOUND;
        }
    }
}
