package com.Controller;

import com.Model.CarsManufactures;
import com.Model.Country;
import com.Service.CountryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Vlad on 7/17/2017.
 */
@Controller
@RequestMapping(value="/countries")
public class CountriesController {
    @Autowired
    CountryService countryService;

    @RequestMapping(value = "")
    public String countries(Model model) {
        model.addAttribute("countryList", countryService.list());
        return "countries";
    }

    @RequestMapping(value="/update", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int updateCountryToEdit(@ModelAttribute("id") Long id, @ModelAttribute("countryName") String countryName){
        try {
            Country country = new Country(countryName);
            Country country1 = countryService.getCountryById(id);
            country.setCountryId(country1.getCountryId());
            BeanUtils.copyProperties(country, country1);
            countryService.saveAndFlush(country1);
            return HttpServletResponse.SC_OK;
        } catch (Exception e) {
            return HttpServletResponse.SC_FORBIDDEN;
        }
    }

    @RequestMapping(value="/delete", method = RequestMethod.GET, produces = "application/json")
    public String deleteCountry(@RequestParam("countryId") String countryId, Model model) {
        try {
            Long idToDelete = Long.parseLong(countryId);
            countryService.delete(idToDelete);
            return "redirect:/countries";
        } catch (Exception e) {
            return "THIS COUNTRY APPEARS IN SALES RECORD";
        }
    }

    @RequestMapping(value = "/editCountry", method = RequestMethod.GET)
    public String displayCountryToEdit(@RequestParam("countryId") Long id, Model model) {
        Country country = countryService.getCountryById(id);
        model.addAttribute("countryToEdit",country);
        return "editCountry";
    }

    @RequestMapping(value= "/addCountry", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int AddUser(@ModelAttribute("countryName") String countryName) {
        try {
            Country country = new Country(countryName);
            countryService.saveAndFlush(country);
            return HttpServletResponse.SC_OK;
        } catch(Exception e) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
    }
}
