package com.Controller;

import com.Model.Cars;
import com.Model.CarsManufactures;
import com.Model.Country;
import com.Repository.CarRepository;
import com.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.Request;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.*;

import static java.lang.Integer.parseInt;

/**
 * Created by Vlad on 7/13/2017.
 */
@Controller
public class IndexController {
    @Autowired
    private CarService carService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CountryService countryService;

    @RequestMapping(value = "/addCar", method = RequestMethod.POST)
    public void addCar(HttpServletRequest request,HttpServletResponse response, Model model) throws IOException {
        String carName = request.getParameter("carName");
        int carProducerId = parseInt((request.getParameter("carProducerId")));
        int carProductionYear = parseInt(request.getParameter("carProductionYear"));
        double carRating = Double.parseDouble(request.getParameter("carRating"));

        CarsManufactures carsManufactures = companyService.getCompanyById(Long.valueOf(carProducerId));
        Cars cars = new Cars(carName, carsManufactures, carProductionYear, carRating);
        carService.saveAndFlush(cars);
        response.sendRedirect("/index");

    }

    @RequestMapping(value = "/addCarProducer", method = RequestMethod.POST)
    public void addCarProducer(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String companyName = request.getParameter("companyName");
        String companyAdress = request.getParameter("companyAdress");
        String companyPostalCode  = request.getParameter("companyPostalCode");

        CarsManufactures carsManufactures = new CarsManufactures(companyName, companyAdress, companyPostalCode);
        companyService.saveAndFlush(carsManufactures);
        response.sendRedirect("/index");
    }

    @RequestMapping(value = "/addCountry", method = RequestMethod.POST)
    public void addCountry(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String countryName = request.getParameter("countryName");

        Country country = new Country(countryName);
        countryService.saveAndFlush(country);
        response.sendRedirect("/index");


    }




    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String messages(Model model) {

        model.addAttribute("carsList", carService.list());
        model.addAttribute("countryList", countryService.list());
        model.addAttribute("carManufacturesList", companyService.list());

        return "index";
    }
}