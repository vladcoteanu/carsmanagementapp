package com.Controller;

import com.Model.Cars;
import com.Model.CarsManufactures;
import com.Service.CompanyServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Vlad on 7/17/2017.
 */
@Controller
@RequestMapping(value="/companies")
public class CompaniesController {
    @Autowired
    CompanyServiceImpl companyService;

    @RequestMapping(value = "")
    public String companies(Model model) {
        model.addAttribute("companyList", companyService.list());
        return "companies";
    }

    @RequestMapping(value="/delete", method = RequestMethod.GET, produces = "application/json")
    public String deleteCompany(@RequestParam("companyId") String companyId, Model model) {
        try {
            Long idToDelete = Long.parseLong(companyId);
            companyService.delete(idToDelete);
            return "redirect:/companies";
        } catch (Exception e) {
            return "THIS COMPANY STILL HAS CARS IN RECORDS";
        }
    }

    @RequestMapping(value="/update", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int updateCompanyToEdit(@ModelAttribute("id") Long id, @ModelAttribute("companyName") String carName,
                                   @ModelAttribute("companyAdress") String companyAdress,
                                   @ModelAttribute("companyPostalCode") String companyPostalCode){
        try {
            CarsManufactures carsManufactures = new CarsManufactures(carName, companyAdress, companyPostalCode);
            CarsManufactures carsManufactures1 = companyService.getCompanyById(id);
            carsManufactures.setCompanyId(carsManufactures1.getCompanyId());
            BeanUtils.copyProperties(carsManufactures, carsManufactures1);
            companyService.saveAndFlush(carsManufactures1);
            return HttpServletResponse.SC_OK;
        } catch (Exception e) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
    }

    @RequestMapping(value = "/editCompany", method = RequestMethod.GET)
    public String displayCompanyToEdit(@RequestParam("companyId") Long id, Model model) {
        CarsManufactures carsManufactures = companyService.getCompanyById(id);
        model.addAttribute("companyToEdit",carsManufactures);
        return "editCompany";
    }

    @RequestMapping(value= "/addCompany", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int AddUser(@ModelAttribute("companyName") String companyName, @ModelAttribute("companyAdress") String companyAdress,
                        @ModelAttribute("companyPostalCode") String companyPostalCode) {

        try {
            CarsManufactures carsManufactures = new CarsManufactures(companyName, companyAdress, companyPostalCode);
            companyService.saveAndFlush(carsManufactures);
            return HttpServletResponse.SC_OK;
        } catch (Exception e) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
    }
}
