package com.Controller;

import com.Model.Cars;
import com.Model.CarsManufactures;
import com.Model.Country;
import com.Service.CarService;
import com.Service.CountryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Vlad on 7/17/2017.
 */
@Controller
@RequestMapping("/sale")
public class SaleController {
    @Autowired
    CarService carService;

    @Autowired
    CountryService countryService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String navigateToSale(Model model) {
        model.addAttribute("carList", carService.list());
        model.addAttribute("countryList", countryService.list());
        return "sale";
    }

    @RequestMapping(value= "/editSale", method = RequestMethod.GET)
    public String editToSale(@RequestParam("carId") String carId, @RequestParam("countryId") String countryId, Model model) {
        model.addAttribute("countryToEdit",countryService.getCountryById(Long.parseLong(countryId)));
        model.addAttribute("carToUpdate", carService.getCarById(Long.parseLong(carId)));
        model.addAttribute("countryList", countryService.list());
        return "editSale";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteSaleRecord(@RequestParam("carId") Long carId, @RequestParam("countryId") Long countryId) {
        try{
        Cars car = carService.getCarById(carId);
        Set<Country> countrySet2 = car.getCountrySet();
        Iterator<Country> iterator = countrySet2.iterator();
        while (iterator.hasNext()) {
            Country country = iterator.next();
            if (country.getCountryId() == countryId) {
                iterator.remove();
            }
        }
        car.setCountrySet(countrySet2);
        carService.flush(car);
        return "redirect:/sale";
         } catch (Exception e) {
            return "SALE CANNOT BE DELETED";
        }
    }

    @RequestMapping(value = "/addReport", method = RequestMethod.POST)
    @ResponseBody
    public int addReport(@ModelAttribute("carName") String carName, @ModelAttribute("countryName") String countryName) {
        try{
            int i = 0;
            long carId = 0;
            while(carName.charAt(i) != '.') {
                carId = carId * 10 + carName.charAt(i) - 48;
                i++;
            }

            i = 0;
            long countryId = 0;
            while(carName.charAt(i) != '.') {
                countryId = countryId * 10 + countryName.charAt(i) - 48;
                i++;
            }

            Cars car = carService.getCarById(carId);
            Country country = countryService.getCountryById(countryId);

            car.addCountryInSet(country);
            country.addCarInSet(car);

            carService.saveAndFlush(car);
            countryService.saveAndFlush(country);

            return HttpServletResponse.SC_OK;
        } catch (Exception e) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
    }

    @RequestMapping(value="/update", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public int updateSaleToEdit(@ModelAttribute("carId") Long id, @ModelAttribute("countryInfo") String countryInfo,
                                @ModelAttribute("countryId") Long countryId){
        try {
            int i = 0;
            long prodId = 0;
            while(countryInfo.charAt(i) != '.') {
                prodId = prodId * 10 + countryInfo.charAt(i) - 48;
                i++;
            }
            Cars car = carService.getCarById(id);
            Set<Country> countrySet2 = car.getCountrySet();
            Iterator<Country> iterator = countrySet2.iterator();
            while (iterator.hasNext()) {
                Country country = iterator.next();
                if (country.getCountryId() == countryId) {
                    iterator.remove();
                }
            }
            countrySet2.add(countryService.getCountryById(prodId));
            car.setCountrySet(countrySet2);
            carService.flush(car);
            return HttpServletResponse.SC_OK;
        } catch (Exception e) {
            return HttpServletResponse.SC_NOT_FOUND;
        }
    }

}
