package com.Model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Vlad on 7/13/2017.
 */
@Entity
@Table(name="cars")
public class Cars {
    @Id
    @GeneratedValue
    private Long carId;

    @Column(name="car_name", table = "cars")
    private String carName;

    //@Column(name="car_producer_id", table = "cars")
    @ManyToOne(fetch = FetchType.LAZY )
    @JoinColumn(name="car_producer_id", table = "cars")
    private CarsManufactures carProducer;

    @Column(name="car_production_year", table = "cars")
    private int carProductionYear;

    @Column(name="car_rating", table = "cars")
    private double carRating;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "salesRegister", joinColumns = {
            @JoinColumn(name = "car_id")},
            inverseJoinColumns = {@JoinColumn(name = "country_id")})
    @JsonManagedReference
    Set<Country> countrySet;

    public void addCountryInSet(Country country) {
        countrySet.add(country);
    }

    public CarsManufactures getCarProducer() {
        return carProducer;
    }

    public void setCarProducer(CarsManufactures carProducer) {
        this.carProducer = carProducer;
    }

    public Set<Country> getCountrySet() {
        return countrySet;
    }

    public void setCountrySet(Set<Country> countrySet) {
        this.countrySet = countrySet;
    }

    public Cars() {

    }

    public Cars(String carName, CarsManufactures carProducerId, int carProductionYear, double carRating) {
        this.carName = carName;
        this.carProducer = carProducerId;
        this.carProductionYear = carProductionYear;
        this.carRating = carRating;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public CarsManufactures getCarProducerId() {
        return carProducer;
    }

    public void setCarProducerId(CarsManufactures carProducerId) {
        this.carProducer = carProducerId;
    }

    public int getCarProductionYear() {
        return carProductionYear;
    }

    public void setCarProductionYear(int carProductionYear) {
        this.carProductionYear = carProductionYear;
    }

    public double getCarRating() {
        return carRating;
    }

    public void setCarRating(double carRating) {
        this.carRating = carRating;
    }

    public String getSpecification() {
        return getCarName() + " produced in " + getCarProductionYear() + " by producer with id: " + getCarProducerId();
    }
}
