package com.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by Vlad on 7/21/2017.
 */
@Entity
public class User {
    @Id
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
