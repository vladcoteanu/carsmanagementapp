package com.Model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Vlad on 7/13/2017.
 */
@Entity
@Table(name="carsmanufactures")
public class CarsManufactures {
    @Id
    @GeneratedValue

    @Column(name="company_id")
    private Long companyId;

    @Column(name="company_name")
    private String companyName;

    @Column(name="company_adress")
    private String companyAdress;

    @Column(name="company_postal_code")
    private String postalCode;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "carProducer")
    private Set<Cars> carsSet;

    public CarsManufactures() {};

    public CarsManufactures(String companyName, String companyAdress, String postalCode, Set<Cars> carsSet) {
        this.companyName = companyName;
        this.companyAdress = companyAdress;
        this.postalCode = postalCode;
        this.carsSet = carsSet;
    }

    public CarsManufactures(String companyName, String companyAdress, String postalCode) {
        this.companyName = companyName;
        this.companyAdress = companyAdress;
        this.postalCode = postalCode;
        Set<Cars> carSet = new HashSet<Cars>(0);
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAdress() {
        return companyAdress;
    }

    public void setCompanyAdress(String companyAdress) {
        this.companyAdress = companyAdress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getSpecification() {
        return getCompanyName() + " located at " + getCompanyAdress();
    }

    public Set<Cars> getCarsSet() {
        return this.carsSet;
    }

    public void setCarsSet(Set<Cars> carsSet) {
        this.carsSet = carsSet;
    }

}
