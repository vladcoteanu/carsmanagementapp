package com.Model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Vlad on 7/13/2017.
 */
@Entity
@Table(name="country")
public class Country {
    @Id
    @GeneratedValue

    @Column(name="country_id")
    private Long countryId;

    @Column(name="country_name")
    private String countryName;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "countrySet")
    private Set<Cars> carSet;

    public void addCarInSet(Cars car) {
        carSet.add(car);
    }

    public Country(String countryName) {
        this.countryName = countryName;
    }

    public Country() {};

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long country) {
        this.countryId = country;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getSpecification() {
        return getCountryName();
    }
}
