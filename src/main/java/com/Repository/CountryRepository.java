package com.Repository;

import com.Model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Vlad on 7/13/2017.
 */
@Repository
public interface CountryRepository extends JpaRepository<Country, Long>{
}
