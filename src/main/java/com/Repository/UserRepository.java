package com.Repository;

import com.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Vlad on 7/21/2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User, String>{

    @Transactional(readOnly = true)
    User getByUsername(String username);

}
