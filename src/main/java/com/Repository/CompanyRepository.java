package com.Repository;

import com.Model.CarsManufactures;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Vlad on 7/13/2017.
 */
@Repository
public interface CompanyRepository extends JpaRepository<CarsManufactures, Long>{
}
