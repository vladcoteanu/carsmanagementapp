package com.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.Model.Cars;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Vlad on 7/13/2017.
 */
@Repository
public interface CarRepository extends JpaRepository<Cars, Long> {
    List<Cars> findAll();
}
