DROP TABLE IF EXISTS salesRegister;
DROP TABLE IF EXISTS country;
DROP TABLE IF EXISTS cars;
DROP TABLE IF EXISTS carsManufactures;




create table carsManufactures (
	company_id int not null auto_increment,
    company_name varchar(255) not null,
    company_adress varchar(500) not null,
    company_postal_code varchar(100),
    primary key(company_id)
);

create table cars(
	car_id int not null auto_increment,
    car_name varchar(255) not null,
    car_producer_id int not null,
    car_production_year int not null,
    car_rating double,
    primary key(car_id),
    foreign key(car_producer_id) references carsManufactures(company_id)
);

create table country(
	country_id int not null auto_increment,
    country_name varchar(255) not null,
    primary key(country_id)
);

create table salesRegister(
	sale_id int not null auto_increment,
    car_id int not null,
    country_id int not null,
    primary key(sale_id),
    foreign key(country_id) references country(country_id)
)