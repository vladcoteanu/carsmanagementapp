function showAddCar(){
	document.getElementById('add_car').style.display='block';
	document.getElementById('display_cars').style.display='none';
	return false;
}

function showListCars(){
	document.getElementById('add_car').style.display='none';
	document.getElementById('display_cars').style.display='block';
	document.getElementById('succesMessage').style.display='none';
    document.getElementById('failMessage').style.display='none';
	return false;
}

function addCar() {
    var carName = $('#insertCarName').val();
    var carProducerId = $('#insertCarProducerId').val();
    var carProductionYear = $('#insertCarProductionYear').val();
    var carRating = $('#insertCarRating').val();


    if(document.getElementById("insertCarName").value == "") {
        document.getElementById('succesMessage').style.display='none';
        document.getElementById('failMessage').innerHTML="Please insert a car name";
        document.getElementById('failMessage').style.display='block';
        return false;
    }

    if(document.getElementById("insertCarProducerId").value == "") {
        document.getElementById('succesMessage').style.display='none';
        document.getElementById('failMessage').innerHTML="Please insert a car producer Id";
        document.getElementById('failMessage').style.display='block';
        return false;
    }

    if(document.getElementById("insertCarProductionYear").value == "") {
        document.getElementById('succesMessage').style.display='none';
        document.getElementById('failMessage').innerHTML="Please insert a car production year";
        document.getElementById('failMessage').style.display='block';
        return false;
    }

    if(document.getElementById("insertCarRating").value == "") {
            carRating = 5;
    }

    $.ajax({
        type: "POST",
        url: "/cars/addCar",
        data: {
            "carName" : carName,
            "carProducerId" : carProducerId,
            "carProductionYear" : carProductionYear,
            "carRating": carRating
        },
        dataType: "json"
    }).done(function() {
                    $('#insertCarName').val("");
                    $('#insertCarProducerId').val("");
                    $('#insertCarProductionYear').val("");
                    $('#insertCarRating').val("");
                    $('#failMessage').hide();
                    $('#succesMessage').text("Car inserted");
                    $('#succesMessage').show();
                }).fail(function() {
                    $('#succesMessage').hide();
                    $('#failMessage').text("Error");
                    $('#failMessage').show();
                });
};

//function editTo(id) {
//    $.ajax({
//                type: "GET",
//                url: "/cars/editCar/"+id
//            });
//};

function updateCar(id) {
    var carId = id;
    var carName = $('#updateCarName').val();
    var carProducerId = $('#updateCarProducerId').val();
    var carProductionYear = $('#updateCarProductionYear').val();
    var carRating = $('#updateCarRating').val();


        if(document.getElementById("updateCarName").value == "") {
            document.getElementById('succesUpMessage').style.display='none';
            document.getElementById('failUpMessage').innerHTML="Please insert a car name";
            document.getElementById('failUpMessage').style.display='block';
            return false;
        }

        if(document.getElementById("updateCarProducerId").value == "") {
            document.getElementById('succesUpMessage').style.display='none';
            document.getElementById('failUpMessage').innerHTML="Please insert a car producer Id";
            document.getElementById('failUpMessage').style.display='block';
            return false;
        }

        if(document.getElementById("updateCarProductionYear").value == "") {
            document.getElementById('succesUpMessage').style.display='none';
            document.getElementById('failUpMessage').innerHTML="Please insert a car production year";
            document.getElementById('failUpMessage').style.display='block';
            return false;
        }

        if(document.getElementById("updateCarRating").value == "") {
                carRating = 5;
        }

        $.ajax({
            type: "POST",
            url: '/cars/update',
            data: {
                "id" : carId,
                "carName" : carName,
                "carProducerId" : carProducerId,
                "carProductionYear" : carProductionYear,
                "carRating": carRating
            },
            dataType: "json"
        }).done(function() {
                        $('#returnButton').show();
                    }).fail(function() {
                        $('#updateCarName').val="22";
                        $('#failMessage').text("Error");
                        $('#failMessage').show();
                    });
};

function deleteCar(idToPass) {
    var dummy = 0;
    $.ajax({
                type: "DELETE",
                url: '/cars/delete',
                data: {
                    carId: idToPass
                }
            });
};