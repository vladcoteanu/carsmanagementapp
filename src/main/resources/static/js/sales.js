function showAddReport(){
	document.getElementById('add_sale_report').style.display='block';
	document.getElementById('display_reports').style.display='none';
	return false;
}

function showListReports(){
	document.getElementById('add_sale_report').style.display='none';
	document.getElementById('display_reports').style.display='block';
	document.getElementById('succesMessage').style.display='none';
	document.getElementById('failMessage').style.display='none';
	return false;
}

function addReport() {
    var countryId = $('#selectCountry').val();
    var carId = $("#selectCarName").val();

    $.ajax({
        type: "POST",
        url: "/sale/addReport",
        data: {
            "carName" : carId,
            "countryName" : countryId
        },
        dataType: "json"
    }).done(function() {
        $('#failMessage').hide();
        $('#succesMessage').text("Succesfully inserted report");
        $('#succesMessage').show();
    }).fail(function() {
        $('#succesMessage').hide();
        $('#failMessage').text("Error");
        $('#failMessage').show();
    });

};

function updateReport(id, countryId) {
    var carId = id;
    var countryInfo = $('#selectCountry').val();
    var ctId = countryId;

        $.ajax({
            type: "POST",
            url: '/sale/update',
            data: {
                "carId" : carId,
                "countryId": ctId,
                "countryInfo" : countryInfo
            },
            dataType: "json"
        }).done(function() {
                        $('#returnButton').show();
                    }).fail(function() {
                        $('#failMessage').text("Error");
                        $('#failMessage').show();
                    });
};