function showAddCountry(){
	document.getElementById('add_country').style.display='block';
	document.getElementById('display_country').style.display='none';
	return false;
}

function showListCountries(){
	document.getElementById('add_country').style.display='none';
	document.getElementById('display_country').style.display='block';
	document.getElementById('succesMessage').style.display='none';
	document.getElementById('failMessage').style.display='none';
	return false;
}

function addCountry() {
    var countryName = $('#insertCountryName').val();

    if(document.getElementById('insertCountryName').value == "") {
        document.getElementById('succesMessage').style.display='none';
        document.getElementById('failMessage').innerHTML="Please insert a country name";
        document.getElementById('failMessage').style.display='block';
        return false;
    }

    $.ajax({
        type: "POST",
        url: "/countries/addCountry",
        data: {
            "countryName" : countryName,
        },
        dataType: "json"
    }).done(function() {
        $('#insertCountryName').val("");
        $('#failMessage').hide();
        $('#succesMessage').text("Country inserted");
        $('#succesMessage').show();
    }).fail(function() {
        $('#succesMessage').hide();
        $('#failMessage').text("Error");
        $('#failMessage').show();
    });

};

function updateCountry(id) {
    var countryId = id;
    var countryName = $('#updateCountryName').val();

        if(document.getElementById("updateCountryName").value == "") {
            document.getElementById('succesUpMessage').style.display='none';
            document.getElementById('failUpMessage').innerHTML="Please insert a car name";
            document.getElementById('failUpMessage').style.display='block';
            return false;
        }

        $.ajax({
            type: "POST",
            url: '/countries/update',
            data: {
                "id" : countryId,
                "countryName" : countryName,
            },
            dataType: "json"
        }).done(function() {
                        $('#returnButton').show();
                    }).fail(function() {
                        $('#failMessage').text("Error");
                        $('#failMessage').show();
                    });
};